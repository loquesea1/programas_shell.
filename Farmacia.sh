 #Raul_Hernandez_Lopez
#freeenergy1975@gmail.com
#Miercoles 17 de septiembre del 2020

#Calcular el descuento y el monto a pagar por un medicamento cualquiera 
#en una farmacia si todos los medicamentos tienen un descuento del 35%.

#Declaracion de variables
Precio_Descuento=0
Precio_Medicamento=0
Descuento=.27
Medicamento=0

printf "\nNombre del medicamento :"
read Medicamento
printf "\nPrecio real del medicamento :"
read Precio_Medicamento
Precio_Descuento=$(echo "scale=2;$Precio_Medicamento*$Descuento" | bc -l)
Descuento=$(echo "scale=2;$Precio_Medicamento-$Precio_Descuento" | bc -l)
printf "\n$Medicamento\nDescuento [$ $Descuento ]\nMonto a pagar [$ $Precio_Descuento ]"

