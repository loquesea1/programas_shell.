#!/bin/bash
#Raul hernandez lopez(Berserker)
#freeenergy1975@gmail.com
#15 octubre del 2020

#Una persona recibe un préstamo de $10,000.00 de un banco y desea 
#saber cuánto pagará de interés, si el banco le cobra una tasa del
#27% anual.
clear 
#Declaracion de variables
Tasa_Interes=.27
#Captura datos
printf "\nAño de prestamo :"
read Fecha_Prestamo
printf "\nAño actual :"
read Fecha_Actual
printf "\nMonto Prestado $"
read Prestamo
Tiempo_Transcurrido=$(($Fecha_Actual-$Fecha_Prestamo))
#Calcula el monto a pagar
for((x = 1; x <= $Tiempo_Transcurrido; x++)) do
    Fecha_Prestamo=$(($Fecha_Prestamo+1))
    interes=$(echo "scale=2; $Prestamo*$Tasa_Interes" | bc -l)
    Prestamo=$(echo "scale=2; $Prestamo+$interes" | bc -l)
    echo "Año [ $Fecha_Prestamo ] El monto a pagar [ $Prestamo ]"
done

