#!/bin/bash
#RAÚL HERNÁDEZ LÓPEZ
#freeenergy1975@gmail.com
#17 de novembre del 2020

#Un vendedor recibe un sueldo base más un 10% extra por comisión de sus ventas,
#el vendedor desea saber cuánto dinero obtendrá por concepto de comisiones por 
#las tres ventas que realiza en el mes y el total que recibirá en el mes tomando 
#en cuenta su sueldo base y comisiones.

comision=0

#Recolecta datos
printf "\nSueldo fijo :"
read sueldo_fijo
printf "\nNumero de ventas realizadas este mes :"
read numero_ventas
#Determina el monto correspondiente por concepto de ventas
for((x = 0; x < $numero_ventas; x++)) do 
    printf "\nIngresa el valor de la venta :" 
    read venta
    comision=$(echo "scale=2;$comision+($venta*.10)" | bc -l)
done
sueldo_total=$( echo "scale=2;$comision+$sueldo_fijo" | bc -l)
printf "Monto a cobrar por concepto de ventas :$ $comision\n"

printf "Monto total a cobrar :$ $sueldo_total\n"
