#RAÚL HERNÁNDEZ LÓPEZ
#freeenergy1975@gmail.com
#16 de Noviembre del 2020

#Escriba un algoritmo que, dado el número de horas trabajadas por un empleado y el sueldo 
#por hora, calcule el sueldo total de ese empleado.
#Tenga en cuenta que las horas extras se pagan el doble.

total_horas=0
#Recopila datos
printf "\nIngresa el sueldo por hora :"
read sueldo_hora
printf "\nHora de entrada(formato de 24 hrs):"
read entrada
printf "\nHora salida(formato de 24 hrs):"
read salida
#Determina el total de horas trabajadas
total_horas=$(($salida-$entrada))
#total_horas=$(echo "scale=2;$salida-$entrada" | bc -l)
#Determina el sueldo correspondiente segun las horas laboradas
if [ $total_horas -le 9 ]; then
   sueldo_normal=$(echo "scale=2;$sueldo_hora*$total_horas" | bc -l)
   printf "\nMonto a cobrar $ $sueldo_normal\n"
#Determina el sueldo con horas etras    
elif [ $total_horas -gt 8 ]; then
   horas_extras=$(($total_horas-8))
   extra=$(($sueldo_hora*2))
   pago_extra=$(echo "scale=2;$horas_extras*$extra" | bc -l)
   #$sueldo_extras=$(echo "scale=2;$pago_extra+($sueldio_hora*8)" | bc -l)
   sueldo_mas_extras=$((($sueldo_hora*8)+$pago_extra)) 
   #Imprime resultados
   printf "\nMonto a cobrar $ $sueldo_mas_extras\n"
fi
