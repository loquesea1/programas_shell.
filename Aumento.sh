#RAÚL HERNÁNDEZ LÓPEZ
#freeenergy1975@gmail.com
#16 de Noviembre del 2020

#Calcular el nuevo salario de un empleado si obtuvo un incremento del 8% sobre su salario actual y
#un descuento de 2.5% por servicios.

#Recopila datos
printf "\nIngresa el monto de tu salario actual:"
read sueldo_actual
#Determina el valor del nuevo sueldo, es descuento del 2.5% y el sueldo final
nuevo_sueldo=$(echo "scale=2;$sueldo_actual+($sueldo_actual*0.08)" | bc -l)
descuento=$(echo "scale=2;$nuevo_sueldo*0.025" | bc -l)
sueldo_final=$(echo "scale=2;$nuevo_sueldo-$descuento" | bc -l)
#Imprme resultados
printf "sueldo con aumento $ $nuevo_sueldo\nDescuento $ $descuento\nMonto a cobrar $ $sueldo_final\n"
