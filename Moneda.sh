#RAÚL HERNÁNDEZ LÓPEZ
#16 de novimbre del 2020
#freeenergy1975@gmail.com

#Calcular el cambio de monedas en dólares y euros al ingresar cierta cantidad en pesos.
#(Tipo de cambio Euros:22.50 ).

dolares=22.09
euros=21.91

printf "\nIngresa el número de pesos que tienes :"
read pesos
printf "\nA que tipo de monedsa quieres hacer la conversion: \n1)Dolares \n2)Euros\n"
read seleccion 

if [ $seleccion -eq 1  ]; then
	conversion=$(echo "scale=2;$pesos/$dolares" | bc -l)
	printf "\nla cantidad de pesos que tienes es igual a $conversion dolares\n"
elif [ $seleccion -eq 2 ]; then 
	conversion=$(echo "scale=2;$pesos/$euros" | bc -l)
	printf "\nla cantidad de pesos que tienes es igual a $conversion euros\n"
fi
