#!/bin/bash
#Raul hernandez lopez(Berserker)
#freeenergy1975@gmail.com
#15 octubre del 2020
#modificado 16 de noviembre del 2020

#Obtener la edad de una persona en meses, si se ingresa su edad en años y meses.
#Ejemplo: Ingresado 3 años 4 meses debemostrar 40 meses.

#Declaracion de variables
printf "\nMes de nacimiento(1-12) :"
read mes_nacimiento
printf "\nAño de nacimiento ejemplo (2001) :"
read year_nacimiento
printf "\nMes actual :"
read mes_actual
printf "\nAño actual :"
read year_actual
#Calcula el numero de meses a lo largo de la vida del usuario
edad_meses=$(echo "scale=2;($mes_actual-$mes_nacimiento)+($year_actual-$year_nacimiento)*12" | bc -l)
#Imprime resultados
printf "\nTu edad en meses es igual :$edad_meses\n"
