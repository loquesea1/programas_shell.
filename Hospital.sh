#RAÚL HERNÁNDEZ LÓPEZ
#freeenergy1975@gmail.com
#16 de Noviembre del 2020

#En un hospital existen 3 áreas: Urgencias, Pediatría y Traumatología. El presupuesto anual del
#hospital se reparte de la siguiente manera: Pediatría 42% y Traumatología 21%.

printf "\nPresupuesto total :"
read presupuesto_total
presupuesto_pediatria=$(echo "scale=2;$presupuesto_total*0.42" | bc -l)
presupuesto_traumatologia=$(echo "scale=2;$presupuesto_total*0.12" | bc -l)
presupuesto_urgencias=$(echo "scale=2;$presupuesto_total-($presupuesto_pediatria+$presupuesto_traumatologia)" | bc -l)

printf "la distribucion anual del presupuesto queda de la siguiente manera :
\nPediatria $ $presupuesto_pediatria\nTraumatologia $ $presupuesto_traumatologia
Urgencias $ $presupuesto_urgencias\n"
